package com.worshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worshop.dao.IEspecialistaDAO;
import com.worshop.models.especialista;

import com.worshop.service.IEspecialistaService;
@Service
public class EspecialistaService implements IEspecialistaService {

	@Autowired
	IEspecialistaDAO service;
	@Override
	public especialista persist(especialista e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<especialista> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public especialista findBYId(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public especialista merge(especialista e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}

}
