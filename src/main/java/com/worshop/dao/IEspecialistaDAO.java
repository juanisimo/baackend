package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worshop.models.especialista;
@Repository
public interface IEspecialistaDAO extends JpaRepository<especialista,Integer> {

}
