package com.worshop.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Paciente {
	
	@Id
	@Column(name="id",nullable=false,unique=true)
	private Integer id;
	@Column(name="nombrePaciente",length=200,nullable=false,unique=true)
	private String nombrePaciente;
	@Column(name="rutPaciente",length=200,nullable=false,unique=true)
	private String rutPaciente;
	@Column(name="emailPaciente",length=100,nullable=false,unique=true)
	private String emailPaciente;
	@Column(name="direccionPaciente",length=100,nullable=false,unique=true)
	private String direccionPaciente;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombrePaciente() {
		return nombrePaciente;
	}
	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}
	public String getRutPaciente() {
		return rutPaciente;
	}
	public void setRutPaciente(String rutPaciente) {
		this.rutPaciente = rutPaciente;
	}
	public String getEmailPaciente() {
		return emailPaciente;
	}
	public void setEmailPaciente(String emailPaciente) {
		this.emailPaciente = emailPaciente;
	}
	public String getDireccionPaciente() {
		return direccionPaciente;
	}
	public void setDireccionPaciente(String direccionPaciente) {
		this.direccionPaciente = direccionPaciente;
	}
	
	
	

}
