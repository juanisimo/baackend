package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.worshop.models.Paciente;


@Repository
public interface IPacienteDAO extends JpaRepository<Paciente,Integer> {

}
