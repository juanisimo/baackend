package com.worshop.controller;

import java.util.ArrayList;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.worshop.models.especialista;
import com.worshop.service.IEspecialistaService;
import com.worshop.serviceimpl.EspecialistaService;


@RestController
@RequestMapping("/especialista")
public class EspecialistaController {
	
	@Autowired
	IEspecialistaService service;
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<especialista>> listar(){
		List<especialista> especialistas=new ArrayList<>();
		especialistas=service.getAll();
		return new ResponseEntity<List<especialista>>(especialistas,HttpStatus.OK);
		
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public especialista registrar(@RequestBody especialista v) {
		return service.persist(v);
	}
	
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<especialista> listarId(@PathVariable("id") Integer id){
		especialista especialista=service.findBYId(id);
		return new ResponseEntity<especialista>(especialista,HttpStatus.OK);
	}
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>actualizar(@Valid @RequestBody especialista especialidad){
		service.merge(especialidad);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	public void eliminar(@PathVariable Integer id) {
		especialista espec=service.findBYId(id);
		//if(espec==null) {
	//		throw new ModeNotFoundException("ID"+id);
	//	}else {
			service.delete(id);
		//}
	}

}
